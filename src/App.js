import React, { Component, Fragment, useState } from "react";
import styled from "styled-components";
// import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import PlayIcon from "@material-ui/icons/PlayArrow";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import 'whatwg-fetch';


const App = () => {
	const [dbName, setDbName] = useState('');
	const [tableName, setTableName] = useState('');
	const [tableFields, setTableFields] = useState([]);
	const [queryString, setQueryString] = useState('');
	const [deleteQueryString, setDeleteQueryString] = useState('');
	const [updateQueryString, setUpdateQueryString] = useState('');
	const [updateChangeString, setUpdateChangeString] = useState('');
	const [rows, setRows] = useState([]);
	const [insertDoc, setInsertDoc] = useState({});
	document.body.style.background = "#022840";

	const makeRequest = async (data) => {
		let result = await fetch('/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		});
		let finalResult = await result.json()
		console.log(data, finalResult);
		return finalResult;
	}

	const getFields = async () => {
		const reqData = { "command" : "get_fields" };
		let results = await makeRequest(reqData);
		setTableFields(results.fields);
		return;
	}
	const selectDatabase = async () => {
		const reqData = { "command" : "select database", "db_name" : dbName }
		return await makeRequest(reqData);
	}
	const selectTable = async () => {
		const reqData = { "command" : "select table", "table_name" : tableName }
		await makeRequest(reqData);
		return await getFields();
	}
	// Pentru delete db si delete table nu facem momentan, nici pentru create.
	const insert = async () => {
		console.log(insertDoc);
		const reqData = { "command" : "insert", "doc" : insertDoc};
		return await makeRequest(reqData);
	}
	const query = async() => {
		const reqData = { "command" : "query", "query_string" : queryString};
		let results = await makeRequest(reqData);
		let _rows = [];
		for(let idx = 0; idx < results.results.rows.length; ++idx) {
			let row = {}
			let field_idx = 0;
			for(let w of results.results.rows[idx].split('|')) {
				row[tableFields[field_idx].name] = w;
				field_idx += 1;
			}
			_rows.push(row)
		}
		setRows(_rows);
		return;
	}
	const remove = async() => {
		const reqData = { "command" : "delete", "query_string" : deleteQueryString};
		await makeRequest(reqData);
		alert("Done deleting!");
		return;
	}
	const update = async() => {
		const reqData = { "command" : "update", "query_string" : updateQueryString, "update_string": updateChangeString };
		await makeRequest(reqData);
		alert("Done updating!");
		return;
	}
	// const run = async () => {
	// 	console.log("run coomand"); 
	// 	let _ = await this.selectDatabase("myDB");
	// 	_ = await this.selectTable("studenti");
	// 	console.log(await this.getFields());
	// 	let id = 0;
	// 	const createData = (name, calories, fat, carbs, protein) => {
	// 		id += 1;
	// 		return { id, name, calories, fat, carbs, protein };
	// 	};
	// 	const rows = [
	// 		createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
	// 		createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
	// 		createData("Eclair", 262, 16.0, 24, 6.0),
	// 		createData("Cupcake", 305, 3.7, 67, 4.3),
	// 		createData("Gingerbread", 356, 16.0, 49, 3.9)
	// 	];
	// 	this.setState({ rows });
	// };

	return (
		<Container>
			<Fragment>
				<CustomCard>
					<CustomCardContent justifyContent="space-between">
						<CustomTextField
							id="set-db-name"
							label="Select Database"
							type="search"
							margin="normal"
							variant="outlined"
							onChange={e => setDbName(e.target.value)}
						/>
						<CustomButton
							variant="contained"
							color="#03A688"
							onClick={selectDatabase}
						>
							<PlayIcon />
						</CustomButton>
					</CustomCardContent>
					<CustomCardContent justifyContent="space-between">
						<CustomTextField
							id="set-table-name"
							label="Select Table"
							type="search"
							margin="normal"
							variant="outlined"
							onChange={e => setTableName(e.target.value)}
						/>
						<CustomButton
							variant="contained"
							color="#03A688"
							onClick={selectTable}
						>
							<PlayIcon />
						</CustomButton>
					</CustomCardContent>
					<CustomCardContent justifyContent="space-between">
						<CustomTextField
							id="query-string"
							label="Query"
							type="search"
							margin="normal"
							variant="outlined"
							onChange={e => setQueryString(e.target.value)}
						/>
						<CustomButton
							variant="contained"
							color="#03A688"
							onClick={query}
						>
							<PlayIcon />
						</CustomButton>
					</CustomCardContent>

					<CustomCardContent justifyContent="space-between">
						<CustomTextField
							id="delete-string"
							label="Delete"
							type="search"
							margin="normal"
							variant="outlined"
							onChange={e => setDeleteQueryString(e.target.value)}
						/>
						<CustomButton
							variant="contained"
							color="#03A688"
							onClick={remove}
						>
							<PlayIcon />
						</CustomButton>
					</CustomCardContent>

					
					<CustomCardContent justifyContent="space-between">
						<CustomTextField
							id="update-query"
							label="Update (query)"
							type="search"
							margin="normal"
							variant="outlined"
							onChange={e => setUpdateQueryString(e.target.value)}
						/>
						<CustomTextField
							id="update-change"
							label="Update (change)"
							type="search"
							margin="normal"
							variant="outlined"
							onChange={e => setUpdateChangeString(e.target.value)}
						/>
						<CustomButton
							variant="contained"
							color="#03A688"
							onClick={update}
						>
							<PlayIcon />
						</CustomButton>
					</CustomCardContent>

					<CustomCardContent justifyContent="space-between">
						{tableFields.map((heading, index) => (
							<CustomTextField 
								id={`insert_${heading.name}`}
								key={`insert_${heading.name}`}
								label={heading.name}
								type="search"
								margin="normal"
								variant="outlined"
								onChange={e => setInsertDoc({...insertDoc, [heading.name]: e.target.value})}
							/>
						))}
						<CustomButton
							variant="contained"
							color="#03A688"
							onClick={insert}
						>
							<PlayIcon />
						</CustomButton>
					</CustomCardContent>

					<CardContent>
						{rows.length <= 0 ? (
							<CustomCardContent justifyContent="center">
								<Typography>No items</Typography>
							</CustomCardContent>
						) : (
							<Table>
								<TableHead>
									<TableRow>
										{tableFields.map((heading, index) => (
											<TableCell key={index}>{heading.name}</TableCell>
										))}
									</TableRow>
								</TableHead>
								<TableBody>
									{rows.map((row, rowIdx) => (
										<TableRow key={rowIdx}>
											{tableFields.map((heading, colIdx) => (
												<TableCell key={rowIdx+"_"+colIdx}>{row[heading.name]}</TableCell>
											))}
										</TableRow>
									))}
								</TableBody>
							</Table>
						)}
					</CardContent>
				</CustomCard>
			</Fragment>
		</Container>
	);

}

const Container = styled.div`
	display: flex;
	/* background-color: #03a688; */
	justify-content: center;
	padding: 100px;
	color: #6bb3f2;
`;
const CustomCard = styled(Card)`
	&& {
		background-color: #03a688;
		width: 95%;
		padding: 24px;
	}
`;
const CustomCardContent = styled(CardContent)`
	&& {
		display: flex;
		color: #186b8c;
		/* background-color: yellowgreen; */
		justify-content: space-between;
		${props =>
			props.justifyContent && `justify-content: ${props.justifyContent}`};

		align-items: center;
	}
`;
const CustomTextField = styled(TextField)`
	width: 90%;
`;
const CustomButton = styled(Button)`
	padding-left: 12;
	margin-right: 12;
	height: 55px;
	background-color: #03a688;
`;
export default App;
